import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Todolist from './modules/Todo/Todolist';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { CookiesProvider } from 'react-cookie';

ReactDOM.render(
  <CookiesProvider>
    <React.StrictMode>
      <Router>
        <Switch>
          <Route path="/" exact component={App} />
          <Route path="/Todo" component={Todolist} />
        </Switch>
      </Router>
    </React.StrictMode>
  </CookiesProvider>,
  document.getElementById('root')
);
