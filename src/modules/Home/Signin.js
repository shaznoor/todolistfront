import React from 'react';
import login from './detailsignin';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import axios from 'axios';
import { useCookies } from 'react-cookie';
import { useHistory } from 'react-router-dom';

const schema = yup.object().shape({
  email: yup.string().required().email(),
  password: yup.string().required().min(8).max(15),
});

const Signin = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const history = useHistory();
  const [cookies, setCookie, removeCookie] = useCookies(['auth']);

  const onSubmit = async (e) => {
    const data = { email: e.email, password: e.password };
    try {
      const res = await axios.post(
        'https://stark-lake-86296.herokuapp.com/user/auth/login',
        data
      );
      setCookie('auth', res.data['access_token'], { path: '/' });
      history.push('/Todo');
    } catch (error) {
      alert('Failed');
    }
  };

  return (
    <>
      <h1>SIGN IN</h1>
      <div className="form">
        <form onSubmit={handleSubmit(onSubmit)}>
          {login.inputs.map((e, key) => {
            return (
              <>
                <div key={key}>
                  <p>
                    <label>{e.label}</label>
                  </p>
                  <p>
                    <input
                      className="input"
                      type={e.type}
                      value={e.value}
                      {...register(e.name)}
                    />
                  </p>
                  <p className="message">{errors[e.name]?.message}</p>
                </div>
              </>
            );
          })}
          <>
            <button type="submit">Sign In</button>
          </>
        </form>
      </div>
    </>
  );
};

export default Signin;
