import React from 'react';
import signup from './detailsignup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import axios from 'axios';

const schema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().required().email(),
  password: yup.string().required().min(8).max(15),
  cpassword: yup.string().required().min(8).max(15),
});

const Signup = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (e) => {
    const data = { name: e.name, email: e.email, password: e.password };
    try {
      const res = await axios.post('https://stark-lake-86296.herokuapp.com/user/', data);
      alert('Signup Successful');
      window.location.reload();
    } catch (error) {
      alert('Failed');
      window.location.reload();
    }
  };

  return (
    <>
      <h1>SIGN UP</h1>
      <div className="form">
        <form onSubmit={handleSubmit(onSubmit)}>
          {signup.inputs.map((e, key) => {
            return (
              <>
                <div key={key}>
                  <p>
                    <label>{e.label}</label>
                  </p>
                  <p>
                    <input
                      className="input"
                      type={e.type}
                      value={e.value}
                      {...register(e.name)}
                    />
                  </p>
                  <p className="message">{errors[e.name]?.message}</p>
                </div>
              </>
            );
          })}
          <>
            <button type="submit">Sign Up</button>
          </>
        </form>
      </div>
    </>
  );
};

export default Signup;
