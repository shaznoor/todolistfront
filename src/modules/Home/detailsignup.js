const signup = {
  inputs: [
    {
      label: 'Name',
      name: 'name',
      type: 'text',
    },
    {
      label: 'Email',
      name: 'email',
      type: 'email',
    },
    {
      label: 'Password',
      name: 'password',
      type: 'password',
    },
    {
      label: 'Confirm Password',
      name: 'cpassword',
      type: 'password',
    },
  ],
};

export default signup;
