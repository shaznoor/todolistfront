import React from 'react';
import { RiCloseCircleLine } from 'react-icons/ri';
import axios from 'axios';
import { useCookies, withCookies, Cookies } from 'react-cookie';
import { useHistory } from 'react-router-dom';

const SubTodo = (props) => {
  const [cookies, getCookie, setCookie, removeCookie] = useCookies(['auth']);
  const history = useHistory();

  function mainFunction(maintodoid, subtodoid) {
    const token = props.cookies.get('auth');
    const data = { maintodoid, subtodoid };
    async function deleteSubTodo() {
      try {
        const res = await axios.put(
          'https://stark-lake-86296.herokuapp.com/user/deletesubtodo',
          data,
          {
            headers: { Authorization: 'Bearer ' + token },
          }
        );
        window.location.reload();
      } catch (error) {
        history.push('/');
      }
    }
    return deleteSubTodo;
  }
  return (
    <div className="todo-row-sub">
      <div>{props.subtodotitle}</div>
      <div className="icons">
        <RiCloseCircleLine
          className="delete-icon"
          onClick={mainFunction(props.maintodoId, props.subtodoid)}
        />
      </div>
    </div>
  );
};

export default withCookies(SubTodo);
