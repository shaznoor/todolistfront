import React from 'react';
import MainTodo from './MainTodo';
import SubTodo from './SubTodo';

const RenderTodo = (props) => {
  return (
    <>
      <MainTodo title={props.maintitle} maintodoId={props.maintodoId} />
      {props.SubTodo.map((value) => {
        return (
          <SubTodo
            subtodotitle={value.title}
            subtodoid={value['_id']}
            maintodoId={props.maintodoId}
          />
        );
      })}
    </>
  );
};

export default RenderTodo;
