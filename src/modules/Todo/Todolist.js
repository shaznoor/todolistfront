import { Button } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import MyVerticallyCenteredModal from './popup';
import axios from 'axios';
import RenderTodo from './RenderTodo';
import { useCookies, withCookies, Cookies } from 'react-cookie';
import { useHistory } from 'react-router-dom';

const Todolist = (props) => {
  const [modalShow, setModalShow] = React.useState(false);
  const [input, setInput] = useState([]);
  const [cookies, getCookie, setCookie, removeCookie] = useCookies(['auth']);
  const history = useHistory();

  useEffect(() => {
    async function getAllTodo() {
      const token = props.cookies.get('auth');
      try {
        const res = await axios.get('https://stark-lake-86296.herokuapp.com/user/gettodo', {
          headers: { Authorization: 'Bearer ' + token },
        });
        setInput(res.data);
      } catch (error) {
        history.push('/');
      }
    }
    getAllTodo();
  }, []);

  return (
    <>
      <div class="text-center border border-dark p-3 mb-4">
        <Button variant="btn btn-primary" onClick={() => setModalShow(true)}>
          CREATE TODOLIST
        </Button>
        <Button
          variant="btn btn-primary"
          onClick={() => {
            props.cookies.remove('auth');
            history.push('/');
          }}
        >
          SIGN OUT
        </Button>
      </div>

      {input.map((value) => {
        return (
          <RenderTodo
            maintitle={value.title}
            SubTodo={value.SubTodo}
            maintodoId={value['_id']}
          />
        );
      })}

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        title={'TODOLIST'}
        type={'maintodo'}
        maintodoId={null}
      />
    </>
  );
};

export default withCookies(Todolist);
