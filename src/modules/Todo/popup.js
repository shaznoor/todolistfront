import axios from 'axios';
import { Modal, Button } from 'react-bootstrap';
import { useRef } from 'react';
import { useCookies, withCookies } from 'react-cookie';
import { useHistory } from 'react-router-dom';

const MyVerticallyCenteredModal = (props) => {
  const [cookies, getCookie, setCookie, removeCookie] = useCookies(['auth']);
  const ref = useRef();
  const history = useHistory();

  function mainFunction(type, ref, maintodoId) {
    async function putMainTodo() {
      const token = props.cookies.get('auth');
      console.log(token);
      let data = {};
      let url = '';
      const title = ref.current.value;
      if (type === 'maintodo') {
        url = 'https://stark-lake-86296.herokuapp.com/user/maintodo';
        data = { title: title };
      } else if (type === 'subtodo') {
        url = 'https://stark-lake-86296.herokuapp.com/user/subtodo';
        data = { title: title, maintodoid: maintodoId };
      }
      try {
        const res = await axios.post(url, data, {
          headers: { Authorization: 'Bearer ' + token },
        });
        window.location.reload();
      } catch (error) {
        history.push('/');
      }
    }
    return putMainTodo;
  }

  if (props.user !== null) {
    return (
      <Modal
        {...props}
        size="lg"
        backdrop="static"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            {props.title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input type="text" ref={ref} />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
          <Button onClick={mainFunction(props.type, ref, props.maintodoId)}>
            Submit
          </Button>
        </Modal.Footer>
      </Modal>
    );
  } else {
    return <></>;
  }
};

export default withCookies(MyVerticallyCenteredModal);
