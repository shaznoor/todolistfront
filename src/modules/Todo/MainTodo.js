import axios from 'axios';
import React from 'react';
import { RiCloseCircleLine } from 'react-icons/ri';
import { RiAddLine } from 'react-icons/ri';
import MyVerticallyCenteredModal from './popup';
import { useCookies, withCookies, Cookies } from 'react-cookie';
import { useHistory } from 'react-router-dom';

const MainTodo = (props) => {
  const [cookies, getCookie, setCookie, removeCookie] = useCookies(['auth']);
  const history = useHistory();

  function mainFunction(maintodoid) {
    const token = props.cookies.get('auth');
    const data = { maintodoid };
    async function deleteMainTodo() {
      try {
        const res = await axios.put(
          'https://stark-lake-86296.herokuapp.com/user/deletemaintodo',
          data,
          {
            headers: { Authorization: 'Bearer ' + token },
          }
        );
        window.location.reload();
      } catch (error) {
        history.push('/');
      }
    }
    return deleteMainTodo;
  }
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      <div className="todo-row">
        <div>{props.title}</div>
        <div className="icons">
          <RiCloseCircleLine
            className="delete-icon"
            onClick={mainFunction(props.maintodoId)}
          />
          <RiAddLine className="add-icon" onClick={() => setModalShow(true)} />
        </div>
      </div>
      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        title={'SUB TODOLIST'}
        type={'subtodo'}
        maintodoId={props.maintodoId}
      />
    </>
  );
};

export default withCookies(MainTodo);
