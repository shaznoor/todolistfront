import './App.css';
import logo from './logo.svg';
import Navbar from './modules/Home/Navbar';
import Signin from './modules/Home/Signin';
import Signup from './modules/Home/Signup';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Navbar />
      </header>
      <Signin />
      <Signup />
    </div>
  );
}

export default App;
